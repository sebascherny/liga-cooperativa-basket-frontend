export const fetchData = async (url, method, body = {}) => {
  let user = JSON.parse(localStorage.getItem('user'))
  const options = {
    method,
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + user?.token,
    },
  }
  if (Object.keys(body).length > 0 && method !== 'GET') {
    options.body = JSON.stringify(body)
  }
  try {
    const response = await fetch(url, options)
    const data = await response.json()

    // sleep(2s) Uncomment for testing purposes
    // await new Promise(r => setTimeout(r, 2000))

    return data
  } catch (err) {
    console.error(err)
  }
}
