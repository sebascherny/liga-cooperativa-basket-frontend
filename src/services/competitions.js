import axios from 'axios'
import { json } from 'react-router-dom'
import { apiUrl } from '../config'
export async function getCompetitions(params) {
  let response = axios
    .get(`${apiUrl}competition/` + params)
    .then(response => {
      return response
    })
    .catch(error => {
      throw json({ message: error }, { status: 400 })
    })
  return response
}
