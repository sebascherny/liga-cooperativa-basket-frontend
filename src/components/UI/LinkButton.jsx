import React from 'react'
import { Link } from 'react-router-dom'
import { cn } from '../../lib/utils'
import { buttonVariants } from '../../lib/styles'

const LinkButton = React.forwardRef(
  ({ className, url = '', children, variant, ...props }, ref) => {
    return (
      <Link
        to={url}
        className={cn(buttonVariants({ variant, className }))}
        ref={ref}
        {...props}
      >
        {children}
      </Link>
    )
  },
)
LinkButton.displayName = 'LinkButton'
export default LinkButton
