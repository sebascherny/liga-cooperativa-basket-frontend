import React from 'react'
import { cn } from '@/lib/utils'
import { buttonVariants } from '@/lib/styles'

const Button = React.forwardRef(
  ({ className, type, children, variant, isLoading, ...props }, ref) => {
    return (
      <button
        className={cn(buttonVariants({ variant, className }))}
        ref={ref}
        disabled={isLoading}
        {...props}
        type={type}
      >
        {children}
      </button>
    )
  },
)
Button.displayName = 'Button'
export default Button
