import React from 'react'

const THRow = ({ children }) => {
  return (
    <tr
      scope='row'
      className='px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white'
    >
      {children}
    </tr>
  )
}

export default THRow
