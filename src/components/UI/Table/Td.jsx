import React from 'react'

const Td = ({ children , styles }) => {
  if (styles != undefined){
    return <td className={'px-6 py-4 text-center ' + styles}>{children}</td>
  } else {
    return <td className={'px-6 py-4 text-center '}>{children}</td>
  }
}

export default Td
