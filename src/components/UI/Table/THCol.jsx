import React from 'react'

const THCol = ({ children }) => {
  return (
    <th scope='col' className='px-6 py-3'>
      {children}
    </th>
  )
}

export default THCol
