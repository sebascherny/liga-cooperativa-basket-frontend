import React from 'react'
import THead from './THead'
import THCol from './THCol'
import THRow from './THRow'
import TRow from './TRow'
import Td from './Td'
import Td5 from './Td5'

const Table = ({ children }) => {
  return (
    <table className='w-full text-sm text-left text-gray-500 dark:text-gray-400 overflow-auto'>
      {children}
    </table>
  )
}

Table.THead = THead
Table.THCol = THCol
Table.THRow = THRow
Table.TRow = TRow
Table.Td = Td
Table.Td5 = Td5

export default Table
