import React from 'react'

const Td5 = ({ children , styles }) => {
  if (styles != undefined){
    return <td colSpan={5} className={'px-6 py-4 text-center ' + styles}>{children}</td>
  } else {
    return <td colSpan={5} className={'px-6 py-4 text-center '}>{children}</td>
  }
}

export default Td5
