import React from 'react'

const TRow = ({ children }) => {
  return <tr className='bg-white'>{children}</tr>
}

export default TRow
