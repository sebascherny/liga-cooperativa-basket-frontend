import React from 'react'

const THead = ({ children }) => {
  return (
    <thead className='text-xs text-center text-gray-700 uppercase'>
      {children}
    </thead>
  )
}

export default THead
