import React from 'react'
import { redirect, useLoaderData } from 'react-router-dom'
import Form from '@/components/UI/Form/Form'
import Table from '@/components/UI/Table/Table'


import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'

export async function loaderNewLeague(){
  let leagues = await await fetchData(
    `${apiUrl}${serverEndpoints.league}`,
    'GET'
  )
  return { leagues }
}

export async function newLeagueAction({ request }) {
  let formData = await request.formData()
  const league = Object.fromEntries(formData)
  await fetchData(`${apiUrl}${serverEndpoints.league}`, 'POST', league)
  return redirect('/admin/newleague')
}
 
export function NewLeague() {

  return (
    <>
      <Form >
        <span>League Name</span>
        <Form.Input
          placeholder="League Name"
          aria-label="League Name"
          type="text"
          name="name"
        />
        <span>Year</span>
        <Form.Input
          type="text"
          name="year"
          placeholder="Starting Year"
        />
        <br/>
        <p>
          <Form.Button type="submit">Save</Form.Button>
          <Form.Button type="button">Cancel</Form.Button>
        </p>
      </Form>
      <ListLeagues />
    </>
  )
}

export function ListLeagues() {
  const data = useLoaderData()
  return(
    <Table>
      <Table.THead>
        <Table.TRow>
          <Table.THCol>Nombre</Table.THCol>
          <Table.THCol>Año</Table.THCol>
        </Table.TRow>
      </Table.THead>            
      <tbody>
        {data.leagues.length === 0 ? (
          <p className=''>
                   No hay ligas todavía
          </p>
        ) : (
          data.leagues.map((league,i) =>
            <Table.TRow key={i}>
              <Table.Td  className='align-middle'>{league.name}</Table.Td>
              <Table.Td className='align-middle'>{league.year}</Table.Td>
            </Table.TRow>
          )
        )}
      </tbody>
    </Table>
  )
}