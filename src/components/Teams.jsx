import React from 'react'
import { Link, redirect, useLoaderData } from 'react-router-dom'
import Form from '@/components/UI/Form/Form'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'
import { getTeams } from '../services/teams'
import ListTeams from './TeamsList'


export async function loaderNewTeam() {
  let leagues = await await fetchData(
    `${apiUrl}${serverEndpoints.league}`,
    'GET'
  )
  let teams = await fetchData(
    `${apiUrl}${serverEndpoints.team}?expanded=true`,
    'GET'
  )
  
  return {leagues, teams}
}

export async function newTeamAction({ request }) {
  let formData = await request.formData()
  const team = Object.fromEntries(formData)
  if(team.photo){
    let photoBase64 = await new Promise(resolve => {
      const fileReader = new FileReader()
      fileReader.onload = function (event) {
        resolve(event.target.result)
      }
      fileReader.onerror = error => {
        console.log(error)
      }
      fileReader.readAsDataURL(team.photo)
    })
    if(photoBase64 != 'data:application/octet-stream;base64,')
      team.photo = photoBase64
  }
  else
    team.photo = ''
  await fetchData(
    `${apiUrl}${serverEndpoints.team}`,
    'POST',
    team
  )
  return redirect('/admin/newteam')
}

export async function loaderTeams() {
  let teams = await getTeams('')
  return { teams}
}

export async function loader({ params }) {
  let teams = await getTeams(params.teamId)
  return teams
}

export default function Team() {
  const team = useLoaderData()
  return (
    <>
      <div id='sidebar'>
        <h1>Equipo: {team.data.name}</h1>
        <h1>
          Photo: <img src={team.data.photo} alt='Logo'></img>
        </h1>
        <div>
          <Link to='/admin/newteam'>
            <button>New Team</button>
          </Link>
        </div>
      </div>
    </>
  )
}

export function EditTeam() {
  const team = useLoaderData()

  return (
    <Form method='post' id='contact-form'>
      <p>
        <span>Name</span>
        <input
          placeholder='Name'
          aria-label='Name'
          type='text'
          name='name'
          defaultValue={team.name}
        />
      </p>
      <label>
        <span>Delegado</span>
        <input
          type='text'
          name='delegate'
          placeholder='Id de usuario delegado'
          defaultValue={team.delegate}
        />
      </label>
      <label>
        <span>Logo</span>
        <input
          placeholder='https://example.com/avatar.jpg'
          aria-label='Avatar URL'
          type='text'
          name='avatar'
          defaultValue={team.photo}
        />
      </label>
      <p>
        <button type='submit'>Save</button>
        <button type='button'>Cancel</button>
      </p>
    </Form>
  )
}

export function NewTeam() {
  let data = useLoaderData()
  return (
    <>
      <Form>
        <span>Name</span>
        <Form.Input placeholder='Name' aria-label='Name' type='text' name='name' required />
        <br />
        <span>Logo</span>
        <Form.Input type='file' name='photo' />
        <br />
        <Form.Select name="leagueId">
          {data.leagues.map(league => (
            <option key={league.id} value={league.id}>
              {league.name}
            </option>
          ))}
        </Form.Select>
        <p>
          <Form.Button type='submit'>Save</Form.Button>
          <Form.Button type='button'>Cancel</Form.Button>
        </p>
      </Form>
      <ListTeams/>
    </>
  )
}
