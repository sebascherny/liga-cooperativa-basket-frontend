import React from 'react'

const CourtCard = ({ court }) => {
  return (
    <div
      key={court.id}
      className='border-solid border-2 border-orange-500 rounded bg-white p-0'
    >
      <div className=' py-2 text-center bg-orange-100'>
        <h5 className='font-semibold'>{court.name}</h5>
      </div>
      <div className='pt-2 pb-4 px-2 text-center'>
        <strong className='font-semibold'>Dirección: </strong>
        <span className='mt-2'>{court.direction}</span>
        {court.lights ? (
          <div className='mt-2'>
            <strong className='font-semibold'>Luces: </strong>
            {court.lightTimeout}
          </div>
        ) : null}
        <strong className='font-semibold'>Estado: </strong>
        <span className='mt-2'>{court.status}</span>
      </div>
    </div>
  )
}

export default CourtCard
