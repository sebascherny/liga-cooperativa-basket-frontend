import React from 'react'
import { Link } from 'react-router-dom'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'

async function deleteGame(gameId) {
    let competitionTeams = await fetchData(
      `${apiUrl}${serverEndpoints.game}${gameId}`,
      'DELETE',
    )
    window.location.reload(false);
}

const GameCard = ({ game }) => {
  return (
    <div className="max-w-sm mx-2 my-2">
      <div
        key={game.id}
        className='flex h-full flex-col border-solid border-2 border-orange-500 rounded bg-white'
      >
        <div className='py-2 text-center bg-orange-100'>
          { game.finished ?
            <h5 className='font-semibold'>Subir FairPlay</h5>
            : <h5 className='font-semibold'>Subir Resultado</h5>
          }
        </div>
        <div className='px-4 py-3 text-center'>
          <div className='mt-2'>
            <strong className='text-muted'>
              {game.competition.name}
            </strong>
          </div>
            <div className='mt-2'>
              <strong className='text-muted align-left'>
                {game.date.split('.')[0].split('T')[0].split('-')[2]}/
                {game.date.split('.')[0].split('T')[0].split('-')[1]}/
                {game.date.split('.')[0].split('T')[0].split('-')[0]}
              </strong>
              <strong className='text-muted align-right'>
                {' '}
                {game.date.split('.')[0].split('T')[1]}
              </strong>
            </div>
          <div className='grid grid-cols-3 items-center mt-2'>
          <div className='mt-2'>
              <img
                src={`data:image/jpeg;base64, ${game.local.photo}`}
                className='mx-auto'
                style={{ width: '3rem', height: '3rem' }}
                alt='Logo'
              />
              <strong>{game.local.name}</strong>
              {game.finished ? <>
                <br></br>
                  <strong>{game.localScore}</strong> </>
                : <></>
              }
          </div>
          <div className='mt-2'>
            <span className='font-bold'>vs</span>
          </div>
          <div className='mt-2'>
            <img
              src={`data:image/jpeg;base64, ${game.visitant.photo}`}
              className='img-thumbnail mx-auto'
              style={{ width: '3rem', height: '3rem' }}
              alt='Logo'
            />
            <strong>{game.visitant.name}</strong>
            {game.finished ? <>
              <br></br>
                <strong>{game.visitantScore}</strong> </>
              : <></>
            }
          </div>
          </div>
          <div className='mt-4'>
            <strong className='text-muted'>
              {game.court.name}
            </strong>
            <div className='text-muted mt-2'>
              {game.court.direction}
            </div>
          </div>
        </div>
        <div className='pt-2 pb-4 px-2 text-center'>
        {!game.finished ?
          <>
          <button type='button' className='text-red-500' onClick={() => deleteGame(game.id)}>
            Borrar
          </button>
          <Link
            key={game.id}
            to={'/update-game/score'}
            state={{ game }}
            >
            <button type='button' className='bg-white border-solid border-2 border-red-500 rounded tex-red-500'>
              Subir Resultado
            </button>
          </Link>
          </>
           :
           <Link
           key={game.id}
           to={'/update-game/feedback'}
           state={{ game }}
           >
           <button type='button' className='bg-white border-solid border-2 border-red-500 rounded tex-red-500'>
             Subir Fairplay
           </button>
           </Link>
         }
        </div>
      </div>
    </div>
  )
}

export default GameCard
