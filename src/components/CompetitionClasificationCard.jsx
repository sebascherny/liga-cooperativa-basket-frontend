
import React from 'react'
import { Link } from 'react-router-dom'
import Table from '@/components/UI/Table/Table'

const CompetitionClasificationCard = ({ competitionClasification }) => {
  var clasificationLink = '/clasification/'+ competitionClasification[0].competition.id
  return (
    <div className="max-w-sm mx-2 my-2 w-full">
      <Link to={clasificationLink} className='-m-1.5 p-1.5 relative '>
        <div
          key={competitionClasification[0].competition.id}
          className='flex h-full flex-col border-solid border-2 border-orange-500 rounded bg-white'
        >
            <h5 className="text-center mb-2 text-xl font-bold text-black-900 py-4 border-b-2 border-solid border-orange-400">{competitionClasification[0].competition.name}</h5>
          <Table>
            <tbody>
              {competitionClasification.length === 0 ? (
               <p className=''>
                 No hay equipos apuntados a esta competición
                </p>
              ) : (
                competitionClasification.map((competitionTeam,i) =>
                  <Table.TRow key={i}>
                    <Table.Td>
                    <div className='flex flex-col	items-center gap-4 md:flex-row'>
                      <img
                        src={`data:image/jpeg;base64, ${competitionTeam.team.photo}`}
                        className='inline-block'
                        style={{ width: '3rem', height: '3rem' }}
                        alt='Logo'
                      /><p className='inline-block ml-4'>{competitionTeam.team.name}</p>
                      </div>
                    </Table.Td>
                    <Table.Td className='align-middle'>Puntos:</Table.Td>
                    <Table.Td className='align-middle'>{competitionTeam.competitionPoints}</Table.Td>
                  </Table.TRow>
                )
              )}
            </tbody>
          </Table>
        </div>
      </Link>
    </div>
  )
}

export default CompetitionClasificationCard
