import React from 'react'
import Header from './Header'
import { Outlet } from 'react-router'
import backgroundImage from '@/asset/background-luismi-color.jpg';

const Layout = () => {
  //
  // const url = url('../asset/background-luismi.jpg')
  // const urlClass = 'bg-['+url+']'
  return (
    <>
      <Header />
      <main className='bg-no-repeat bg-center bg-cover w-full min-h-screen'   style={{backgroundImage: `url(${backgroundImage})`}}>
      <div className='pt-8 mx-auto max-w-7xl w-11/12 '>
          <Outlet />
      </div>
      </main>
    </>
  )
}

export default Layout
