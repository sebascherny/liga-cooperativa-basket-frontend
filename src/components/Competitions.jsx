import React, { useEffect, useState } from 'react'
import { json, redirect, useLoaderData } from 'react-router-dom'
import Form from '@/components/UI/Form/Form'
import Table from '@/components/UI/Table/Table'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'

import { getCompetitions } from '../services/competitions'
import { getTeams } from '../services/teams'

export async function newCompetitionAction({ request }) {
  let formData = await request.formData()
  const competition = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.competition}`,
    'POST',
    competition
  )
  return redirect('/admin/newcompetition')
}

export async function newCompetitionTypeAction({ request }) {
  let formData = await request.formData()
  const competitionType = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'POST',
    competitionType
  )
  return redirect('/admin')
}

export async function AddCompetitionTeamAction({ request }) {
  let formData = await request.formData()
  const addTeam = Object.fromEntries(formData)
  await fetchData(
    `${apiUrl}${serverEndpoints.competition_team}`,
    'POST',
    addTeam
  )
  return redirect('/admin')
}

export async function loaderNewCompetition(){
  let competitionGroups = await fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'GET'
  )
  let competitions = await fetchData(
    `${apiUrl}${serverEndpoints.competition}?expanded=true`,
    'GET'
  )
  
  return { competitionGroups, competitions }
}

export function AddTeamCompetition() {
  const [teams, setTeams] = useState()
  const [competitions, setCompetitions] = useState()

  useEffect(() => {
    getTeams('')
      .then(teams => {
        const html = []
        for (let team of teams.data) {
          html.push(<option key={team.id} value={team.id} >{team.name}</option>)
        }
        setTeams(html)
        getCompetitions('')
          .then(competitions => {
            const html = []
            for (let comp of competitions.data) {
              html.push(<option key={comp.id} value={comp.id} >{comp.name}</option>)
            }
            setCompetitions(html)
          })

      })
      .catch((error) => {
        throw json(
          { message: error},
          { status: 400 }
        )
      })
  }, [])

  return (
    <div id="contact-form">
      <Form method="post" id="add-team-competition-form">
        <label>TEAM</label>
        <select name="teamId">
          <option key="default" value="Select a Team"> -- Select a Team -- </option>
          {teams}
        </select>
        <br/>
        <br/>
        <label>COMPETITON</label>
        <select name="competitionId" >
          <option key="default" value="Select a Competition"> -- Select a Competition -- </option>
          {competitions}
        </select>
        <br/>
        <br/>
        <button type="submit">Save</button>
      </Form>
    </div>
  )
}

export function NewCompetition() {
  const data = useLoaderData()

  const htmlCompGroups = []
  for (let comp of data.competitionGroups) {
    htmlCompGroups.push(<option key={comp.id} value={comp.id} >{comp.name}</option>)
  }

  return (
    <>
      <Form method="post" id="new-user-form">
        <span>Nombre</span>
        <Form.Input
          placeholder="Competition Name"
          aria-label="Name"
          type="text"
          name="name"
        />
        <br/>
        <span>Fase:</span>
        <Form.Select name="competitionGroupId" >
          {htmlCompGroups}
        </Form.Select>
        <br/>
        <p>
          <Form.Button type="submit">Guardar</Form.Button>
          <Form.Button type="button">Cancelar</Form.Button>
        </p>
      </Form>
      <ListCompetitions/>
    </>
  )
}

export function NewCompetitionType() {

  return (
    <div id="contact-form">
      <Form method="post" id="new-user-form">
        <label>
          <span>Competition Type Name</span>
          <input
            placeholder="Competition Type"
            aria-label="Competition Type Name"
            type="text"
            name="name"
          />
        </label>
        <p>
          <button type="submit">Save</button>
          <button type="button">Cancel</button>
        </p>
      </Form>
    </div>
  )
}

export function ListCompetitions(){
  const data = useLoaderData()
  
  return(
    <Table>
      <Table.THead>
        <Table.TRow>
          <Table.THCol>Nombre</Table.THCol>
          <Table.THCol>Fase</Table.THCol>
        </Table.TRow>
      </Table.THead>            
      <tbody>
        {data.competitions.length === 0 ? (
          <p className=''>
                 No hay equipos apuntados a esta competición
          </p>
        ) : (
          data.competitions.map((competition,i) =>
            <Table.TRow key={i}>
              <Table.Td  className='align-middle'>{competition.name}</Table.Td>
              <Table.Td className='align-middle'>{competition.competitionGroupId} - {competition.competitionGroupId.name}</Table.Td>
            </Table.TRow>
          )
        )}
      </tbody>
    </Table>
  )
}