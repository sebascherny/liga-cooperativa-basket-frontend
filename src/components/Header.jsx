import React, { useState } from 'react'
import { Link, NavLink, useLocation } from 'react-router-dom'
import { LogoutIcon, BallIcon } from './Icons'

import { useUserContext } from '../context/UserContext'
import logo from '@/asset/logo-liga.png'

const Navigationbar = () => {
  let location = useLocation()
  const [mobileMenu, setMobileMenu] = useState(true)
  const { logout, user } = useUserContext()

  React.useEffect(() => {
    setMobileMenu(true)
  }, [location])

  const links = [
    {
      url: '/',
      text: 'Clasificación',
    },
    {
      url: '/calendar',
      text: 'Calendario',
    },
    {
      url: '/courts',
      text: 'Canchas',
    },
    {
      url: '/info',
      text: 'Normas',
    },
  ]

  return (
    <header className='bg-white shadow-sm shadow-orange-300 border-solid border-b-2 border-orange-500 rounded'>
      <nav
        className='mx-auto flex max-w-7xl w-11/12 items-center justify-between py-6'
        aria-label='Global'
      >
        <div className='flex lg:flex-1'>
          <Link to='/' className='-m-1.5 p-1.5 relative '>
            <span className='sr-only'>Liga Cooperativa Basket Madrid</span>
            <div className="sm">
              <img src={logo} className="object-contain h-20" alt="logo" />
            </div>
          </Link>
        </div>
        <div className='flex lg:hidden'>
          <button
            onClick={() => setMobileMenu(!mobileMenu)}
            type='button'
            className='-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-orange-600'
          >
            <span className='sr-only'>Open main menu</span>
            <svg
              className='h-8 w-8'
              fill='none'
              viewBox='0 0 24 24'
              strokeWidth='1.5'
              stroke='currentColor'
              aria-hidden='true'
            >
              <path
                strokeLinecap='round'
                strokeLinejoin='round'
                d='M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5'
              />
            </svg>
          </button>
        </div>
        <div className='hidden lg:flex lg:gap-x-12'>
          {links.map((link, i) => {
            return (
              <NavLink
                key={i}
                to={link.url}
                className={({ isActive }) =>
                  (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                  ' font-semibold self-center'
                }
              >
                {link.text}
              </NavLink>
            )
          })}
          {user && user.admin ? (
            <NavLink
              to='/admin'
              className={({ isActive }) =>
                (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                ' font-semibold self-center'
              }
            >
              Administración
            </NavLink>
          ) : null}
          {user && user.teamId != null ? (
            <>
              <NavLink
                to='/update-game'
                className={({ isActive }) =>
                  (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                  ' font-semibold self-center'
                }
              >
                Subir resultados
              </NavLink>
            </>
          ) : null}
          {user ? (
            <>
              <NavLink
                to='/profile'
                className={({ isActive }) =>
                  (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                  ' font-semibold self-center'
                }
              >
                Perfil
              </NavLink>
              <span onClick={logout} className="self-center">
                <LogoutIcon />
              </span>
            </>
          ) : (
            <NavLink
              to='/login'
              className={({ isActive }) =>
                (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                ' font-semibold'
              }
            >
              Login
            </NavLink>
          )}
        </div>
        <div className='hidden lg:flex lg:flex-1 lg:justify-end'></div>
      </nav>
      <div className='lg:hidden' role='dialog' aria-modal='true'>
        <div
          className={'fixed inset-0 z-10 ' + (mobileMenu ? 'hidden' : '')}
        ></div>
        <div
          className={
            'fixed inset-y-0 right-0 z-10 w-full overflow-y-auto bg-white px-6 py-6 sm:ring-1 sm:ring-gray-900/10 ' +
            (mobileMenu ? 'hidden' : '')
          }
        >
          <div className='flex items-center justify-between'>
            <Link to='/' className='-m-1.5 p-1.5'>
              <span className='sr-only'>
                Liga cooperativa de basquet de Madrid
              </span>
              <BallIcon/>
            </Link>
            <button
              onClick={() => setMobileMenu(!mobileMenu)}
              type='button'
              className='-m-2.5 rounded-md p-2.5 text-emerald-500'
            >
              <span className='sr-only'>Close menu</span>
              <svg
                className='h-8 w-8'
                fill='none'
                viewBox='0 0 24 24'
                strokeWidth='1.5'
                stroke='currentColor'
                aria-hidden='true'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  d='M6 18L18 6M6 6l12 12'
                />
              </svg>
            </button>
          </div>
          <div className='mt-6 flow-root'>
            <div className='-my-6 divide-y divide-gray-500/10'>
              <div className='space-y-2 py-6'>
                {links.map((link, i) => {
                  return (
                    <NavLink
                      key={i}
                      to={link.url}
                      className={({ isActive }) =>
                        (isActive
                          ? 'text-emerald-500 hover:bg-orange-50'
                          : 'text-orange-500 hover:bg-emerald-50') +
                        ' font-semibold -mx-3 block rounded-lg px-3 py-2 text-base leading-7'
                      }
                    >
                      {link.text}
                    </NavLink>
                  )
                })}
                {user && user.admin ? (
                  <NavLink
                    to='/admin'
                    className={({ isActive }) =>
                      (isActive
                        ? 'text-emerald-500 hover:bg-orange-50'
                        : 'text-orange-500 hover:bg-emerald-50') +
                      ' font-semibold -mx-3 block rounded-lg px-3 py-2 text-base leading-7'
                    }
                  >
                    Administración
                  </NavLink>
                ) : null}
                {user && user.teamId != null ? (
                  <>
                    <NavLink
                      to='/update-game'
                      className={({ isActive }) =>
                        (isActive
                          ? 'text-emerald-500 hover:bg-orange-50'
                          : 'text-orange-500 hover:bg-emerald-50') +
                        ' font-semibold -mx-3 block rounded-lg px-3 py-2 text-base leading-7'
                      }
                    >
                      Subir resultados
                    </NavLink>
                  </>
                ) : null }
                {user ? (
                  <>
                    <NavLink
                      to='/profile'
                      className={({ isActive }) =>
                        (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                        'font-semibold self-center'
                      }
                    >
                      Perfil
                    </NavLink>
                    <span
                      onClick={logout}
                      className=' font-semibold -mx-3 block rounded-lg px-3 py-2 text-base leading-7'
                    >
                      <LogoutIcon />
                    </span>
                  </>
                ) : (
                  <NavLink
                    to='/login'
                    className={({ isActive }) =>
                      (isActive ? 'text-emerald-500' : 'text-orange-500 ') +
                      ' font-semibold'
                    }
                  >
                    Login
                  </NavLink>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Navigationbar
