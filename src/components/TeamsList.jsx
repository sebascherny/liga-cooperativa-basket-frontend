import React from 'react'
import { useLoaderData } from 'react-router-dom'
import Table from '@/components/UI/Table/Table'


export function ListTeams(){
  const data = useLoaderData()
  
  return(
    <Table>
      <Table.THead>
        <Table.TRow>
          <Table.THCol>Nombre</Table.THCol>
          <Table.THCol>Liga</Table.THCol>
        </Table.TRow>
      </Table.THead>      
      <tbody>
        {data.teams.length === 0 ? (
          <p className=''>
                 No hay equipos
          </p>
        ) : (
          data.teams.map((team,i) =>
            <Table.TRow key={i}>
              <Table.Td  className='align-middle'>{team.name}</Table.Td>
              <Table.Td className='align-middle'>{team.leagueId} - {team.leagueId.name}</Table.Td>
            </Table.TRow>
          )
        )}
      </tbody>
    </Table>
  )
}

export default ListTeams
 