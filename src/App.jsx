import React from 'react'
import './App.css'
import './index.css'
import { useUserContext } from './context/UserContext'
import { RouterProvider } from 'react-router-dom'
import { createBrowserRouter } from 'react-router-dom'
import ErrorPage from '@/error-page'
import {loaderCompetitionClasification} from '@/pages/Competitions/CompetitionClasification'
import {loaderLeaguesClasification} from '@/pages/Leagues/LeaguesClasification'
import {loaderGameStored} from '@/pages/Games/GameStored'
import {loaderGamesCalendar} from '@/pages/Games/GamesCalendar'
import {
  NewTeam,
  newTeamAction,
  EditTeam,
  loader as teamLoader,
  loaderNewTeam,
} from '@/components/Teams'
import {
  NewCompetitionType,
  newCompetitionTypeAction,
  AddTeamCompetition,
  AddCompetitionTeamAction,
  NewCompetition,
  newCompetitionAction,
  loaderNewCompetition,
} from '@/components/Competitions'

import {
  NewCompetitionGroup,
  newCompetitionGroupAction,
  loaderNewCompetitionGroup,
} from '@/pages/Competitions/CompetitionGroup'

import LinkUserTeam, {
  loaderUsersTeams,
  actionLinkUserTeam,
  CreateUserForm,
  newUserAction,
  loaderNewUser,
} from '@/components/Users'
import { NewLeague, newLeagueAction, loaderNewLeague } from '@/components/Leagues'

import { PrivateAdminRoute, PrivateRoute } from '@/components/PrivateRoute'
import { fetchData } from '@/services/services'
import { serverEndpoints } from '@/config'

import {
  Login,
  Register,
  Info,
  Courts,
  NewCourt,
  Team,
  TeamGames,
  GameStored,
  GamesCalendar,
  LeaguesClasification,
  CompetitionClasification,
  Administration,
  ScheduleGame,
  UpdateScoreGame,
  IncompleteGamesList,
  Profile
} from '@/pages/index'
import { newCourtAction } from '@/pages/Court/NewCourt'
import { loaderCourts } from '@/pages/Court/Courts'
import { registerAction } from '@/pages/Register'
import { loginAction } from './pages/Login'
import { loaderScheduleGame, newGameAction } from '@/pages/Games/ScheduleGame'
import { loaderTeamGames } from '@/pages/Games/TeamGames'
import {
  updateScoreGameAction,
  updateFeedbackAction,
} from '@/pages/Games/UpdateScoreGame'
import { loaderIncompleteGamesList } from '@/pages/Games/IncompleteGamesList'
import { actionUpdateProfile } from '@/pages/Users/Profile'
import { apiUrl } from './config'
import Layout from './components/Layout'

export default function App() {
  const userContext = useUserContext()
  const router = createBrowserRouter([
    {
      element: <Layout />,
      children: [
        {
          path: '/',
          element: <LeaguesClasification />,
          loader: loaderLeaguesClasification,
        },
        {
          path: '/calendar',
          element: <GamesCalendar />,
          loader: loaderGamesCalendar,
        },
        {
          path: 'courts',
          element: <Courts />,
          loader: loaderCourts
        },
        {
          path: 'login',
          element: <Login />,
          action: loginAction(userContext),
          errorElement:<Login error={true} />
        },
        {
          path: 'register',
          element: <Register />,
          errorElement: <ErrorPage />,
          action: registerAction(userContext),
        },
        {
          path: 'info',
          element: <Info />,
        },
        {
          path: 'clasification/:clasificationId',
          element: <CompetitionClasification />,
          loader: loaderCompetitionClasification
        },
        {
          path: 'team-games/:competitionId/:teamId',
          element: <TeamGames />,
          loader: loaderTeamGames
        },
        {
          element: <PrivateRoute />,
          errorElement: <ErrorPage />,
          children: [
            {
              path: 'games',
              element: <ScheduleGame />,
              loader: loaderScheduleGame,
              action: newGameAction,
            },
            {
              path: 'games/:gameId',
              element: <GameStored />,
              loader: loaderGameStored,
            },
            {
              path: 'update-game',
              element: <IncompleteGamesList />,
              loader: loaderIncompleteGamesList,
            },
            {
              path: 'update-game/score',
              element: <UpdateScoreGame />,
              action: updateScoreGameAction,
            },
            {
              path: 'update-game/feedback',
              element: <UpdateScoreGame />,
              action: updateFeedbackAction,
            },
            {
              path: 'competition',
              element: <CompetitionClasification />,
              loader: loaderCompetitionClasification,
            },
            {
              path: 'profile',
              element: <Profile />,
              action: actionUpdateProfile,
            },
            {
              path: 'teams/:teamId',
              element: <Team />,
              loader: async ({ params }) =>
                await fetchData(
                  `${apiUrl}${serverEndpoints.team}${params.teamId}`,
                  'GET',
                ),
            },
            {
              path: 'teams/:teamId/edit',
              element: <EditTeam />,
              loader: teamLoader,
            },
            {
              path: 'new-court',
              element: <NewCourt />,
              action: newCourtAction,
            },
            {
              path: 'admin',
              element: <PrivateAdminRoute />,
              children: [
                {
                  index: true,
                  element: <Administration />,
                },
                {
                  path: 'newteam',
                  element: <NewTeam />,
                  loader: loaderNewTeam,
                  action: newTeamAction,
                },
                {
                  path: 'newuser',
                  element: <CreateUserForm />,
                  loader: loaderNewUser,
                  action: newUserAction,
                },
                {
                  path: 'newleague',
                  element: <NewLeague />,
                  loader: loaderNewLeague,
                  action: newLeagueAction,
                },
                {
                  path: 'newcompetition',
                  element: <NewCompetition />,
                  loader: loaderNewCompetition,
                  action: newCompetitionAction,
                },
                {
                  path: 'newcompetitiongroup',
                  element: <NewCompetitionGroup />,
                  loader: loaderNewCompetitionGroup,
                  action: newCompetitionGroupAction,
                },
                {
                  path: 'newcompetitiontype',
                  element: <NewCompetitionType />,
                  action: newCompetitionTypeAction,
                },
                {
                  path: 'addteamcompetition',
                  element: <AddTeamCompetition />,
                  action: AddCompetitionTeamAction,
                },
                {
                  path: 'link-user-team',
                  element: <LinkUserTeam />,
                  loader: loaderUsersTeams,
                  action: actionLinkUserTeam,
                },
              ],
            },
          ],
        },
      ],
    },
  ])
  return <RouterProvider router={router} />
}
