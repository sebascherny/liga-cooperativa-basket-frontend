import React, { useContext, useState } from 'react'
import { redirect } from 'react-router-dom'

const UserContext = React.createContext({})

function UserContextProvider({ children }) {
  const [user, setUser] = useState(() =>
    JSON.parse(window.localStorage.getItem('user')),
  )

  const logout = () => {
    window.localStorage.removeItem('user')
    setUser(null)
    return redirect('/')
  }

  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        logout,
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

const useUserContext = () => {
  return useContext(UserContext)
}

export { useUserContext, UserContextProvider }
