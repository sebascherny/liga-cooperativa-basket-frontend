import React from 'react'
import { Await, defer, useLoaderData, useParams } from 'react-router-dom'
import { serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'
import LinkButton from '@/components/UI/LinkButton'
import { apiUrl } from '@/config'
import Table from '@/components/UI/Table/Table'

export async function loaderTeamGames(props){
  if(!props.params.competitionId || !props.params.teamId){
    return {
      message: 'Se ha producido un error, vuelve a probar.',
      status: 400,
    }
  }
  const games = fetchData(
    `${apiUrl}${serverEndpoints.game}?expanded=true&competitionId=${props.params.competitionId}&teamId=${props.params.teamId}`,
    'GET',
  )
  return defer({games: games})
}

export default function TeamGames() {
  const {competitionId, teamId} = useParams()
  const data = useLoaderData()

  return(
    <>
      {data.status ? (
        <p className='bg-white'>{data.message}</p>
      ): (
        <div className="table-responsive border-solid bg-white border-2 border-orange-500 rounded mt-8 overflow-scroll">
          <Table>
            <Table.THead>
              <Table.TRow>
                <Table.THCol>Fecha</Table.THCol>
                <Table.THCol>Local</Table.THCol>
                <Table.THCol>Resultado</Table.THCol>
                <Table.THCol>Visitante</Table.THCol>
                <Table.THCol>Cancha</Table.THCol>
                <Table.THCol>Completado</Table.THCol>
              </Table.TRow>
            </Table.THead>
            <tbody>
              <React.Suspense fallback={
                <Table.TRow >
                  <td colSpan="7" ><p className='text-center text-lg' >Cargando partidos del equipo...</p></td>
                </Table.TRow>} >
                <Await resolve={data.games}>
                  {(games) => (
                    games.length === 0 ? (
                      <Table.TRow>
                        <Table.Td5>
                    No tienes partidos
                        </Table.Td5>
                      </Table.TRow>
                    ) : (
                      games.map((game,i) =>
                        <Table.TRow key={i}>
                          <Table.Td >
                            {game.date.split('.')[0].split('T')[0].split('-')[2]}/
                            {game.date.split('.')[0].split('T')[0].split('-')[1]}/
                            {game.date.split('.')[0].split('T')[0].split('-')[0]}
                          </Table.Td>
                          <Table.Td>
                            <p className='inline-block mr-4'>{game.local.name}</p>
                            <img
                              src={`data:image/jpeg;base64, ${game.local.photo}`}
                              className='inline-block'
                              style={{ width: '3rem', height: '3rem' }}
                              alt='Logo'
                            />
                          </Table.Td>
                          <Table.Td>
                            {
                              Number(game.local.id) === Number(teamId) ?
                                ( <>
                                  <span className={(Number(game.localScore) > Number(game.visitantScore) ? 'text-emerald-500' : 'text-red-500') + ' font-semibold'}>{game.localScore}</span>-
                                  <span>{game.visitantScore.toString()}</span></>
                                )
                                :
                                (
                                  <><span>{game.localScore.toString()}</span>-
                                    <span className={(game.localScore < game.visitantScore ? 'text-emerald-500' : 'text-red-500') + ' font-semibold'}>{game.visitantScore}</span></>
                                )
                            }
                          </Table.Td>
                          <Table.Td>
                            <img
                              src={`data:image/jpeg;base64, ${game.visitant.photo}`}
                              className='inline-block'
                              style={{ width: '3rem', height: '3rem' }}
                              alt='Logo'
                            /><p className='inline-block ml-4'>{game.visitant.name}</p>
                          </Table.Td>
                          <Table.Td>{game.court.name}</Table.Td>
                          <Table.Td>{game.confirmed ? 'Sí': 'No'}</Table.Td>
                        </Table.TRow>
                      )
                    )
                  )}
                </Await>
              </React.Suspense>
            </tbody>
          </Table>
        </div>
      )}
      <LinkButton url={`/clasification/${competitionId}`} text='Volver' type='submit' variant='subtle' className="bg-white border-solid border-2 border-orange-500 rounded mt-8">
        Volver
      </LinkButton>
    </>
  )

}
