import React from 'react'
import { Await, defer, redirect, useLoaderData } from 'react-router-dom'
import LinkButton from '@/components/UI/LinkButton'
import { apiUrl, serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'

import GameCard from '@/components/GameCard'

export async function loaderIncompleteGamesList() {
  const user = JSON.parse(localStorage.getItem('user'))
  if (!user) {
    return redirect('/login')
  }

  let paramsResult = '?localId=' + user.teamId + '&finished=false&expanded=true'
  let gamesResult = fetchData(
    `${apiUrl}${serverEndpoints.game}${paramsResult}`,
    'GET',
  )
  
  let paramsFairplay = '?visitantId=' + user.teamId + '&finished=true&confirmed=false&expanded=true'
  let gamesFairplay = fetchData(
    `${apiUrl}${serverEndpoints.game}${paramsFairplay}`,
    'GET',
  )

  return defer({ 
    gamesResult : gamesResult,
    gamesFairplay : gamesFairplay
  })
}

export default function IncompleteGamesList() {
  const data = useLoaderData()

  // let now = new Date()
  // for (let game of gamesData.gamesResult){
  //   let date = new Date(game.date)
  //   if (date < now) {
  //     game.hasStarted = true
  //   }
  // }

  return (
    <>
      <div className="flex flex-wrap justify-center">
        <LinkButton url='/games' className="bg-white border-solid border-2 border-orange-500 rounded mt-8">
          Crear partido
        </LinkButton>
      </div>
      <div className="flex flex-wrap justify-center">
        <React.Suspense fallback={<p className="flex h-full flex-col mx-2 my-2 px-2 py-2 bg-white border-solid border-2 border-orange-500 rounded text-center">Cargando partidos con resultado pendiente...</p>} >
          <Await resolve={data.gamesResult}>
            {(gamesResult) => (
              gamesResult.length === 0 ? (
                <div className="flex h-full flex-col mx-2 my-2 px-2 py-2 bg-white border-solid border-2 border-orange-500 rounded text-center">
                  <p>
                No tienes partidos pendientes de subir resultados
                  </p>
                </div>
              ) : (
                gamesResult.map(game =>
                  <GameCard key={game.id} game={game} />
                )
              )
            )}
          </Await>
        </React.Suspense>
      </div>
      <div className="flex flex-wrap justify-center">
        <React.Suspense fallback={<p className="bg-white">Cargando partidos con Fairplay pendiente...</p>} >
          <Await resolve={data.gamesFairplay}>
            {(gamesFairplay) => (
              gamesFairplay.length === 0 ? (
                <div className="flex h-full flex-col mx-2 my-2 px-2 py-2 bg-white border-solid border-2 border-orange-500 rounded text-center">
                  <p>
                No tienes partidos pendientes de subir valoración Fairplay
                  </p>
                </div>
              ) : (
                gamesFairplay.map(game =>
                  <GameCard key={game.id} game={game} />
                )
              )
            )}
          </Await>
        </React.Suspense>
      </div>
    </>
  )
}
