import React from 'react'

import { Await, defer, useLoaderData } from 'react-router-dom'

import { apiUrl, serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'

import Table from '@/components/UI/Table/Table'


export async function loaderGamesCalendar() {
  let paramsResult = '?finished=false&expanded=true'

  let games = fetchData(
    `${apiUrl}${serverEndpoints.game}${paramsResult}`,
    'GET',
  )
  return defer({ games })
}


export default function GamesCalendar() {
  const gamesData = useLoaderData()

  return (
    <>
      <div className="table-responsive border-solid bg-white border-2 border-orange-500 rounded mt-8 overflow-scroll">
        <Table>
          <Table.THead>
            <Table.TRow>
              <Table.THCol>Fecha</Table.THCol>
              <Table.THCol>Cancha</Table.THCol>
              <Table.THCol>Local</Table.THCol>
              <Table.THCol>Visitante</Table.THCol>
              <Table.THCol>Competición</Table.THCol>
            </Table.TRow>
          </Table.THead>
          <React.Suspense fallback={
            <tbody className="bg-white text-lg">
              <Table.TRow><Table.Td5>Cargando Partidos...</Table.Td5></Table.TRow>
            </tbody>} >
            <Await resolve={gamesData.games}>
              {(games) =>
                <tbody>
                  {games.length === 0 ? (
                    <Table.TRow>
                      <Table.Td5>
                        <p className="text-lg">No hay partidos pendientes</p>
                      </Table.Td5>
                    </Table.TRow>
                  ) : (
                    games.map((game,i) =>
                      <Table.TRow key={i}>
                        <Table.Td >
                          {game.date.split('.')[0].split('T')[0].split('-')[2]}/
                          {game.date.split('.')[0].split('T')[0].split('-')[1]}/
                          {game.date.split('.')[0].split('T')[0].split('-')[0]}<br></br>
                          {game.date.split('.')[0].split('T')[1].split(':')[0]}:
                          {game.date.split('.')[0].split('T')[1].split(':')[1]}
                        </Table.Td>
                        <Table.Td>{game.court.name}</Table.Td>
                        <Table.Td>
                          <img
                            src={`data:image/jpeg;base64, ${game.local.photo}`}
                            className='inline-block'
                            style={{ width: '3rem', height: '3rem' }}
                            alt='Logo'
                          /><p className='inline-block ml-4'>{game.local.name}</p></Table.Td>
                        <Table.Td>
                          <img
                            src={`data:image/jpeg;base64, ${game.visitant.photo}`}
                            className='inline-block'
                            style={{ width: '3rem', height: '3rem' }}
                            alt='Logo'
                          /><p className='inline-block ml-4'>{game.visitant.name}</p></Table.Td>
                        <Table.Td>{game.competition.name}</Table.Td>
                      </Table.TRow>
                    )
                  )}
                </tbody>
              }
            </Await>
          </React.Suspense>
        </Table>
      </div>

    </>
  )
}
