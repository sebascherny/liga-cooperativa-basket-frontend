import React from 'react'
import { redirect, useLocation } from 'react-router-dom'
import { updateScoreGame, updateFeedback } from '@/services/games'
import LinkButton from '@/components/UI/LinkButton'
import Form from '@/components/UI/Form/Form'

export async function updateScoreGameAction({ request }) {
  let formData = await request.formData()
  const scoreData = Object.fromEntries(formData)
  await updateScoreGame(scoreData)
  return redirect('/update-game')
}

export async function updateFeedbackAction({ request }) {
  let formData = await request.formData()
  const scoreData = Object.fromEntries(formData)
  await updateFeedback(scoreData)
  return redirect('/update-game')
}

export default function UpdateScoreGame() {
  let { state } = useLocation()

  if (!state.game.finished) {
    return (
      <div
        className='max-w-lg mx-auto card border-solid border-2 border-orange-500 rounded bg-white'
        style={{ width: '100%', textAlign: 'center' }}
      >
        <Form>
          <div className='card-header bg-light'>
            <h5 className='card-title'>Subir Resultado</h5>
          </div>
          <div className='card-body'>
            <div className='card-text text-center'>
              <strong className='text-muted align-left'>
                {state.game.local.name}
              </strong>
            </div>
            <div className='card-text text-center mt-1'>
              <Form.Input
                text='Puntuación'
                type='number'
                id='localScore'
                required
              />
            </div>
            <div className='card-text text-center mt-3'>
              <strong className='text-muted align-left'>
                {state.game.visitant.name}
              </strong>
            </div>
            <div className='card-text text-center mt-1'>
              <Form.Input
                text='Puntuación'
                type='number'
                id='visitantScore'
                required
              />
            </div>
            <div className='card-text text-center mt-3'>
              <strong className='text-muted'>FairPlay</strong>
            </div>
            <div className='card-text text-center mt-1'>
              <Form.Input
                text='Fairplay'
                type='range'
                min='-1'
                max='1'
                defaultValue="0"
                id='visitantFairPlay'
              />
              <div className='flex w-full columns-3'>
                <strong className='text-left w-full'>-1</strong>
                <strong className='text-center w-full'>0</strong>
                <strong className='text-right w-full'>+1</strong>
              </div>
            </div>
            <div className='text-center mt-4'>
              <strong className='text-muted align-left mt-1'>
                ¿El equipo contrario era mixto?
              </strong>
            </div>
            <br/>
            <div className='text-center mb-4'>
              <div className="radio">
                <label>
                  <input name="visitantMixed" type="radio" value='true' required defaultChecked={state.game.visitantMixed == true} />
                  {' '}
                  Sí
                </label>
                <label>
                  <input name="visitantMixed" type="radio" value='false' required defaultChecked={state.game.visitantMixed == false} />
                  {' '}
                  No
                </label>
              </div>
            </div>
            <label style={{ display: 'none' }}>
              <span>Game ID</span>
              <Form.Input
                text='Competition Type Name'
                type='text'
                id='id'
                value={state.game.id}
                readOnly
              />
            </label>
          </div>
          <div className='card-footer bg-transparent border-transparent'>
            <Form.Button type='submit'>Guardar</Form.Button>
            <span className='p-5'>
              <LinkButton url='/update-game' text='Volver' type='submit' variant='subtle'>
                Volver
              </LinkButton>
            </span>
          </div>
        </Form>
      </div>
    )
  } else {
    return (
      <div
        className='max-w-lg mx-auto card border-solid border-2 border-orange-500 rounded bg-white'
        style={{ width: '100%', textAlign: 'center' }}
      >
        <Form>
          <div className='card-header bg-light'>
            <h5 className='card-title'>Subir Fairplay</h5>
          </div>
          <div className='card-body'>
            <div className='card-text text-center'>
              <strong className='text align-left'>
                {state.game.local.name}
              </strong>
            </div>
            <div className='card-text text-center mt-1'>
              <strong className='text-muted align-left'>
                {state.game.localScore}
              </strong>
            </div>
            <div className='card-text text-center mt-3'>
              <strong className='text align-left'>
                {state.game.visitant.name}
              </strong>
            </div>
            <div className='card-text text-center mt-1'>
              <strong className='text-muted align-left'>
                {state.game.visitantScore}
              </strong>
            </div>
            <div className='card-text text-center mt-3'>
              <strong className='text'>FairPlay</strong>
            </div>
            <div className='card-text text-center mt-1'>
              <Form.Input
                text='Fairplay'
                aria-label='Fairplay'
                type='range'
                min='-1'
                max='1'
                defaultValue="0"
                id='localFairPlay'
              />
              <div className='flex w-full columns-3'>
                <strong className='text-left w-full'>-1</strong>
                <strong className='text-center w-full'>0</strong>
                <strong className='text-right w-full'>+1</strong>
              </div>
            </div>
            <div className='card-text text-center mt-3'>
              <strong className='text align-left mt-1'>
                ¿El equipo contrario era mixto?
              </strong>
            </div>
            <div className='text-center mb-3'>
              <div className="radio">
                <label>
                  <input name="visitantMixed" type="radio" value='true' required defaultChecked={state.game.visitantMixed == true} />
                  {' '}
                  Sí
                </label>
                <label>
                  <input name="visitantMixed" type="radio" value='false' required defaultChecked={state.game.visitantMixed == false} />
                  {' '}
                  No
                </label>
              </div>
            </div>
            <label style={{ display: 'none' }}>
              <span>Game ID</span>
              <Form.Input
                text='Competition Type Name'
                type='text'
                id='id'
                value={state.game.id}
                readOnly
              />
            </label>
          </div>
          <div className='card-footer bg-transparent border-transparent'>
            <Form.Button type='submit'>Guardar</Form.Button>
            <span className='p-5'>
              <LinkButton url='/update-game' text='Volver' type='submit' variant='subtle'>
                Volver
              </LinkButton>
            </span>
          </div>
        </Form>
      </div>
    )
  }
}
