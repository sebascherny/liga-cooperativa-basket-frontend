import React, { useState } from 'react'
import { Await, defer, redirect, useLoaderData } from 'react-router-dom'
import { serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'
import { createGame } from '@/services/games'
import Form from '@/components/UI/Form/Form'
import LinkButton from '@/components/UI/LinkButton'
import { apiUrl } from '@/config'


export async function newGameAction({ request }) {
  let formData = await request.formData()
  const game = Object.fromEntries(formData)
  let newGame = await createGame(game)
  return redirect(`/games/${newGame.data.id}`)
}

export async function loaderScheduleGame() {
  let user = JSON.parse(localStorage.getItem('user'))

  let activeCompetitions = fetchData(
    `${apiUrl}${serverEndpoints.competition_team}/get-active-competitions/${user.teamId}`,
    'GET',
  )

  if (activeCompetitions.length === 0) {
    return {
      message: 'You dont have active competitions',
      status: 400,
    }
  }

  return defer({
    activeCompetitions : activeCompetitions,
    user : user
  })
}

export default function ScheduleGame() {
  const loaderScheduleGameData = useLoaderData()
  const user = loaderScheduleGameData.user

  const [courts, setCourts] = useState()
  const [dateSelected, setDateSelected] = useState(false)

  const [teams, setTeams] = useState()
  const [competitionSelected, setCompetitionSelected] = useState(false)

  const changeDate = async e => {
    const date = new Date(e.target.value)
    let url = new URL(
      `${apiUrl}${serverEndpoints.available_courts}`
    )
    url.searchParams.append('date', date.toISOString())
    setCourts(await fetchData(url, 'GET'))
    setDateSelected(e.target.value)
  }

  const changeCompetition = async e => {
    const competitionId = e.target.value

    if (e.target.value == '-1') {
      setTeams([])
      setCompetitionSelected('-1')
    }
    
    let url = new URL(
      `${apiUrl}${serverEndpoints.team}${user.teamId}/${serverEndpoints.team_pending_games}`
    )
    url.searchParams.append('competitionId', competitionId)
    setTeams(await fetchData(url, 'GET'))
    setCompetitionSelected(e.target.value)
  }

  return (
    <>
      <div id='teams' className="justify-center">
        <div
          className='block max-w-lg bg-white border-solid border-2 border-orange-500 rounded mt-8 max-w-lg'
        >
          <div className='card-body'>
            <h5 className='card-title text-center'>Crear partido</h5>
            <Form>
              <div className='input-group mb-3'>
                <React.Suspense fallback={<p className="bg-white">Cargando competiciones activas...</p>} >
                  <Await resolve={loaderScheduleGameData.activeCompetitions}>
                    {(competitions) => (
                      <Form.Select
                        id='competitionId'
                        onChange={changeCompetition}>
                        <option key="-1" value="-1">Elige competición</option>
                        {competitions.map((competition) => {
                          return (
                            <option
                              key={competition.id}
                              value={competition.id}
                            >
                              {competition.name}
                            </option>
                          )
                        })}
                      </Form.Select>
                    )}
                  </Await>
                </React.Suspense>
              </div>
              <div className='input-group mb-3'>
                <label>Elige fecha y hora: </label>
                <Form.Input
                  type='datetime-local'
                  id='date'
                  onChange={changeDate}
                />
              </div>
              {(dateSelected && competitionSelected && teams.length > 0) ? (
                <>
                  <div className='input-group mb-3'>
                    <label>Elige equipo rival: </label>
                    <Form.Select id='visitantId' required>
                      <option key='default'>Elige equipo </option>
                      {teams.map(team => (
                        <option key={team.id} value={team.id}>
                          {team.name}
                        </option>
                      ))}
                    </Form.Select>
                  </div>
                  <div className='input-group mb-3'>
                    <label>Elige la cancha: </label>
                    <Form.Select id='courtId' required>
                      <option key='default'>Elige cancha </option>
                      {courts.map(court => (
                        <option key={court.id} value={court.id}>
                          {court.name}
                        </option>
                      ))}
                    </Form.Select>
                  </div>
                  <div className='card-body mb-3 text-center'>
                    <Form.Button
                      type='submit'
                      className='btn btn-dark text-center'
                    >
                      Confirmar
                    </Form.Button>
                  </div>
                </>
              ) :null}
              {(competitionSelected && teams.length == 0)? (
                <div
                  className='card bg-white border-solid border-2 border-orange-500 rounded mt-8 px-3 py-3'
                  style={{ width: '100%', textAlign: 'left' }}
                >
                  <div className='card-body'>
                    <h5 className='card-title text-center'>No tienes partidos pendientes</h5>
                  </div>
                </div>
              ): null}
              <div className='text-center'>
                <LinkButton
                  url='/'
                  text='Volver'
                  type='submit'
                  variant='subtle'
                >
                Volver
                </LinkButton>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </>
  )
}
