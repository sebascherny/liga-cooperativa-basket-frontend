import React from 'react'
import LinkButton from '../components/UI/LinkButton'

const Administration = () => {
  const nav = [
    {
      link: '/admin/newuser',
      text: 'Crear nuevo usuario',
    },
    {
      link: '/admin/newteam',
      text: 'Crear nuevo equipo',
    },
    {
      link: '/new-court',
      text: 'Crear cancha',
    },
    {
      link: '/admin/newcompetitiontype',
      text: 'Crear tipo de Competición',
    },
    {
      link: '/admin/newleague',
      text: 'Crear Nueva Liga',
    },
    {
      link: '/admin/newcompetitiongroup',
      text: 'Crear Nueva Fase',
    },
    {
      link: '/admin/newcompetition',
      text: 'Crear Nuevo Grupo',
    },
    {
      link: '/admin/addteamcompetition',
      text: 'Añadir equipo a la competición',
    },
    {
      link: '/admin/link-user-team',
      text: 'Vincular Usuario con Equipo',
    },
  ]

  return (
    <div className='grid grid-cols-2 md:grid-cols-4 gap-4 bg-white'>
      {nav.map((elem, i) => (
        <LinkButton
          key={i}
          url={elem.link}
          className='grid items-center bg-white'
          variant={i % 2 ? 'default' : 'subtle'}
        >
          {elem.text}
        </LinkButton>
      ))}
    </div>
  )
}

export default Administration
