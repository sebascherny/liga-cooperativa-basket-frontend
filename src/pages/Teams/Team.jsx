import React from 'react'
import { useLoaderData } from 'react-router'
import Button from '../../components/UI/LinkButton'

const Team = () => {
  const team = useLoaderData()

  return (
    <div className='grid'>
      <div className='text-left'>
        <div className='px-4 py-2 mb-4 border-1 border-r-4 border-orange-300 rounded-sm max-w-[200px]'>
          <div className='mb-2'>
            <p className='font-semibold'>Equipo: {team.name}</p>
          </div>
          <div className='gap-2'>
            <p className='flex'>
              <img className='w-full h-full' src={team.photo} alt='Logo' />
            </p>
          </div>
        </div>
        <Button link={true} url='/admin/newteam' text='Nuevo equipo' />
      </div>
    </div>
  )
}

export default Team
