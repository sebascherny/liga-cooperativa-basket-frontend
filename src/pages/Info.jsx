import React from 'react'

const Info = () => {
  return (
    <div className=' text-left'>
      <div className='card-body border-solid border-2 border-orange-500 rounded mt-8 bg-white py-12 px-4'>
      <div className='max-w-2xl mx-auto'>
        <h1 className='font-bold mb-4'>
          LIGA COOPERATIVA BALONCESTO MADRID – TEMPORADA 2023/2024
        </h1>
        <h2 className='font-semibold mb-2'>Normas de Juego:</h2>
        <p>No hay árbitro/a en los partidos pero sí unas normas de juego:</p>
        <ol className='list-decimal list-inside mb-4'>
          <li>
            La fecha, hora y lugar de los partidos se decide por consenso entre
            los dos equipos.
          </li>
          <li>
            Si uno de los dos equipos no puede asistir y no avisa con al menos 3
            horas de antelación, el otro equipo puede darle el partido por
            perdido.
          </li>
          <li>
            Cualquier equipo puede recurrir primero a la figura de mercenari@
            cuando tenga problemas para jugar un partido pero siempre con el
            consentimiento del otro equipo con el que juega.
          </li>
          <li>
            Se permite jugar en más de un equipo a cualquier jugador@. No hay
            límite para ningún equipo para contar con jugador@s que juegan en
            más de un equipo.
          </li>
          <li>
            Tiempo de juego: 4 cuartos de 12 minutos cada uno. El tiempo se para
            únicamente los últimos 2 minutos del último cuarto y en los tiros
            libres de todos los cuartos.
          </li>
          <li>
            El marcador lo lleva cualquiera de los dos equipos o los dos y
            máximo hay un tiempo muerto para cada equipo en cada cuarto.
          </li>
          <li>
            En caso de lucha prevalece la flecha para decidir qué equipo
            mantiene la posesión.
          </li>
          <li>
            Las faltas o infracciones son pitadas por la persona/equipo que
            comete la infracción. Siguiendo el coarbitraje el otro equipo
            también puede indicarlo y si hay posiciones contrapuestas debatirlo.
          </li>
          <li>
            A partir de las 5 faltas de equipo, hay tiros libres para el otro
            equipo.
          </li>
          <li>
            Si el último jugador/a corta un contraataque sin opción de robar el
            balón se considera falta antideportiva y son 2 tiros libres y
            posesión para el otro equipo (igual con la técnica).
          </li>
        </ol>
        <h3 className='font-semibold mb-2'>Normas de participación:</h3>
        <ol className='list-decimal list-inside mb-4'>
          <li>Hacer, al menos, una tarea en la temporada.</li>
          <li>Asistir a las 3 asambleas generales.</li>
        </ol>
        <div className='mb-4'>
          <h4 className='font-semibold mb-2'>Apercibimiento:</h4>
          <p>
            Actitudes poco deportivas o puntuación muy baja en la clasificación
            del fairplay.
          </p>
        </div>
        <h3 className='font-semibold mb-2'>Expulsión:</h3>
        <ol className='list-decimal list-inside mb-4'>
          <li>No cumplir con las normas de participación</li>
          <li>Intentos de agresión o agresiones</li>
          <li>Acumular más de un apercibimiento</li>
          <li>No respetar los valores de la liga</li>
        </ol>
        <p>
          Todas las normas son susceptibles de eliminarse, modificarse o
          añadirse siempre que cualquier jugador/a lo proponga en una asamblea y
          se apruebe.
        </p>
        <p>Ante cualquier duda, ¡hablemos y consensuemos!</p>
        </div>
      </div>
    </div>
  )
}

export default Info
