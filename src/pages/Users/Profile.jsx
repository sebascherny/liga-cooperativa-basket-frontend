import React from 'react'
import { redirect, useActionData } from 'react-router-dom'
import Form from '@/components/UI/Form/Form'
import { Md5 } from 'ts-md5'
import { apiUrl, serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'

export async function actionUpdateProfile({ request }){
  let user = JSON.parse(localStorage.getItem('user'))
  let formData = await request.formData()
  const data = Object.fromEntries(formData)
  if (data.newPassword != data.repeatPassword){
    return {error: 'Las contraseñas no coinciden'}
  }
  data.oldPassword = Md5.hashStr(data.oldPassword)
  data.newPassword = Md5.hashStr(data.newPassword)
  await fetchData(
    `${apiUrl}${serverEndpoints.user}${user.userId}/update-password`,
    'PUT',
    data
  )
  return redirect('/')
}


const Profile = () => {
  const error = useActionData()

  return (
    <Form>
      <div className='grid gap-4'>
        <h1 className='font-bold'>Cambiar contraseña</h1>
        <br/>
        <Form.Input id='oldPassword' type='password' text='Contraseña Actual' />
        <Form.Input id='newPassword' type='password' text='Nueva Contraseña' />
        <Form.Input
          id='repeatPassword'
          type='password'
          text='Repetir contraseña'
        />
        {error && (
          <p className='text-red-600'>{error.error}</p>
        )}
        <br/>
        <Form.Button type='submit' >
          Cambiar Contraseña
        </Form.Button>
      </div>
    </Form>
  )
}

export default Profile
