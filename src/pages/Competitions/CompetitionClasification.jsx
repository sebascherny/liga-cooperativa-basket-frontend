import React from 'react'
import { Await, defer, useLoaderData } from 'react-router-dom'
import { fetchData } from '@/services/services'
import { Link } from 'react-router-dom'
import LinkButton from '@/components/UI/LinkButton'
import { apiUrl, serverEndpoints } from '@/config'
import Table from '@/components/UI/Table/Table'

export async function loaderCompetitionClasification(props){
  let user = JSON.parse(localStorage.getItem('user'))
  const params = {}

  if(props.params.clasificationId){
    params.competitionId = props.params.clasificationId
  }
  else if (!user.competitions || !user.competitions.length === 0) {
    return {
      message: 'You dont have a team assigned yet. Please ask it',
      status: 400,
    }
  }
  else{
    params.competitionId = user?.activeCompetitionId
  }

  let competitionTeams = fetchData(
    `${apiUrl}${serverEndpoints.competition_teams}${params.competitionId}?expanded=true`,
    'GET',
    params,
  )
  
  return defer({competitionTeams: competitionTeams})
}

export default function CompetitionClasification() {
  const data = useLoaderData()

  return(
    <><div className="table-responsive border-solid border-2 border-orange-500 rounded mt-8 overflow-scroll">
      <Table>
        <Table.THead>
          <Table.TRow>
            <Table.THCol>#</Table.THCol>
            <Table.THCol>Equipo</Table.THCol>
            <Table.THCol>Victorias</Table.THCol>
            <Table.THCol>Derrotas</Table.THCol>
            <Table.THCol>PF</Table.THCol>
            <Table.THCol>PC</Table.THCol>
            <Table.THCol>P</Table.THCol>
          </Table.TRow>
        </Table.THead>
        <tbody>
          <React.Suspense fallback={
            <Table.TRow >
              <td colSpan="7" ><p className='text-center text-lg' >Cargando clasificación...</p></td>
            </Table.TRow>} >
            <Await resolve={data.competitionTeams}>
              {(competitionTeams) => (         
                competitionTeams.map((competitionTeam,i) =>
                  <Table.TRow key={i}>
                    <Table.Td>{i +1}</Table.Td>
                    <Table.Td>
                      <Link to={`/team-games/${competitionTeam.competition.id}/${competitionTeam.team.id}`}>
                        <img
                          src={`data:image/jpeg;base64, ${competitionTeam.team.photo}`}
                          className='inline-block'
                          style={{ width: '3rem', height: '3rem' }}
                          alt='Logo'
                        /><p className='inline-block ml-4'>{competitionTeam.team.name}</p>
                      </Link>
                    </Table.Td>
                    <Table.Td className='align-middle'>{Number(competitionTeam.victories)}</Table.Td>
                    <Table.Td className='align-middle'>{Number(competitionTeam.defeats)}</Table.Td>
                    <Table.Td className='align-middle'>{Number(competitionTeam.pointsScored)}</Table.Td>
                    <Table.Td className='align-middle'>{Number(competitionTeam.pointsReceived)}</Table.Td>
                    <Table.Td className='align-middle'>{Number(competitionTeam.competitionPoints)}</Table.Td>
                  </Table.TRow>
                )
              )}
            </Await>
          </React.Suspense>
        </tbody>
      </Table>
    </div>

    <LinkButton url='/' text='Volver' type='submit' variant='subtle' className="bg-white border-solid border-2 border-orange-500 rounded mt-8">
          Volver
    </LinkButton>
    </>
  )
}
