import React from 'react'
import { redirect, useActionData, useNavigation } from 'react-router-dom'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'
import LinkButton from '@/components/UI/LinkButton'
import Form from '@/components/UI/Form/Form'

export async function newCourtAction({ request }) {
  let formData = await request.formData()
  const court = Object.fromEntries(formData)
  try {
    await fetchData(`${apiUrl}${serverEndpoints.court}`, 'POST', court)
    return redirect('/courts')
  } catch (err) {
    return err.statusText
  }
}

const NewCourt = () => {
  const errorMessage = useActionData()
  const navigation = useNavigation()

  return (
    <>
      <div
        className='max-w-lg border-solid border-2 border-orange-500 rounded bg-white p-0'
      >
        <Form>
          {errorMessage && <h4 style={{ color: 'red' }}>{errorMessage}</h4>}
          <h1 className='font-bold mb-2'>Crear cancha</h1>
          <Form.Input type='text' id='name' text='Nombre' required />
          <Form.Input type='text' id='direction' text='Dirección' required />
          <Form.Input type='text' id='status' text='Estado de la cancha' />
          <Form.Input type='text' id='slightTimeout' text='Apagado de luces' />
          <Form.Checkbox type='checkbox' id='lights' text='Luces' />
          <div className='flex gap-4 mt-4'>
            <Form.Button
              type='submit'
              isLoading={navigation.state === 'submitting'}
            >
              {navigation.state === 'submitting' ? 'Guardando...' : 'Guardar'}
            </Form.Button>
            <LinkButton
              url='/courts'
              text='Guardar'
              type='submit'
              variant='subtle'
            >
          Volver
            </LinkButton>
          </div>
        </Form>
      </div>
    </>
  )
}

export default NewCourt
