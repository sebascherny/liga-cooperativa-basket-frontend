import React from 'react'
import { Await, defer, useLoaderData } from 'react-router'
import CourtCard from '@/components/CourtCard'
import LinkButton from '@/components/UI/LinkButton'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'



export async function loaderCourts(){
  const courts = fetchData(`${apiUrl}${serverEndpoints.court}`, 'GET')
  return defer({courts: courts})
}


export default function Courts() {
  const data = useLoaderData()
  let user = JSON.parse(localStorage.getItem('user'))

  return (
    <section>
      { user ? 
        <LinkButton text='Crear cancha' url='/new-court' className="border-solid border-2 border-orange-500 rounded bg-white">
        Crear cancha
        </LinkButton> : null}
      <div className='grid grid-cols-2 md:grid-cols-3 gap-4 py-4'>
        <React.Suspense fallback={<p className='bg-white'>Cargando canchas...</p>} >
          <Await resolve={data.courts}>
            {(courts) => (         
              courts.length === 0 ? (
                <div className="border-solid border-2 border-orange-500 rounded bg-white px-2 py-2 mx-2 my-2">
             Todavía no hay canchas creadas, te animas a añadir una?
                </div>
              ) : (
                courts.map(court => <CourtCard key={court.id} court={court} />)
              )
            )}
          </Await>
        </React.Suspense>
      </div>
      <div className="table-responsive border-solid bg-white border-2 border-orange-500 rounded mt-8 overflow-scroll">
        <h4>Consulta nuestro mapa de canchas</h4>
        <br />
        <iframe 
          src="https://www.google.com/maps/d/embed?mid=1vpRHDMkhqi5H22SK4CZ6TSh5mys&ehbc=2E312F"
          width="100%"
          height="600"></iframe>
      </div>
    </section>
  )
}
