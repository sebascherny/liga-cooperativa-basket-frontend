import Form from '@/components/UI/Form/Form'
import React, { useEffect } from 'react'
import { Link, redirect, useNavigate } from 'react-router-dom'
import { Md5 } from 'ts-md5'
import { useUserContext } from '@/context/UserContext'
import { fetchData } from '@/services/services'
import { apiUrl, serverEndpoints } from '@/config'

const Register = () => {
  const navigate = useNavigate()
  const { user } = useUserContext()

  useEffect(() => {
    if (user) {
      navigate('/')
    }
  }, [])
  return (
    <section className='relative top-24'>
      <Form
        method='post'
        className='mx-auto max-w-lg bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'
      >
        <h1 className='font-bold mb-2'>Regístrate</h1>
        <Form.Input id='name' type='text' text='Nombre' />
        <Form.Input id='email' text='Email' type='email' />
        <Form.Input id='password' type='password' text='Contraseña' />
        <Form.Input
          id='repeatPassword'
          type='password'
          text='Repetir contraseña'
        />
        <Form.Button type='submit' className='mt-4'>
          Crear usuario
        </Form.Button>
        <p className='mt-4'>
          ¿Tienes cuenta?{' '}
          <Link to='/login' className='text-orange-600'>
            Login
          </Link>
        </p>
      </Form>
    </section>
  )
}

/* eslint-disable indent */
export const registerAction =
  userContext =>
  async ({ request }) => {
    const { setUser } = userContext
    let formData = await request.formData()
    const user = Object.fromEntries(formData)

    user.password = Md5.hashStr(user.password)
    await fetchData(
      `${apiUrl}${serverEndpoints.user}`,
      'POST',
      user
    )
    return redirect('/')
  }

export default Register
