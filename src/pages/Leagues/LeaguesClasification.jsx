import React from 'react'
import { Await, defer, useLoaderData } from 'react-router-dom'
import { serverEndpoints } from '@/config'
import { fetchData } from '@/services/services'
import Form from '@/components/UI/Form/Form'
import { apiUrl } from '@/config'
import CompetitionClasificationCard from '@/components/CompetitionClasificationCard'


export async function loaderLeaguesClasification() {
  let activeCompetitionGroupId = JSON.parse(localStorage.getItem('activeCompetitionGroupId'))

  let activeCompetitionGroups = fetchData(
    `${apiUrl}${serverEndpoints.competition_group}`,
    'GET',
  )

  if ( activeCompetitionGroupId == null ) {

    for( let i = 0; i < activeCompetitionGroups.length; i++){
      if(activeCompetitionGroups[i].active){
        localStorage.setItem('activeCompetitionGroupId', activeCompetitionGroups[i].id)
        activeCompetitionGroupId = activeCompetitionGroups[i].id
        break
      }
    }
  }

  let competitionClasifications = fetchData(
    `${apiUrl}${serverEndpoints.competition_team}clasifications?competitionGroupId=${activeCompetitionGroupId}`,
    'GET',
  )

  return defer({ 
    competitionClasifications : competitionClasifications,
    activeCompetitionGroups : activeCompetitionGroups,
    activeCompetitionGroupId : activeCompetitionGroupId 
  })
}

export default function LeaguesClasification() {
  const data = useLoaderData()

  function changeCompetitionGroup(e) {
    localStorage.setItem('activeCompetitionGroupId', e.target.value)
    window.location.reload(false)
  }

  return(
    <>
      <React.Suspense fallback={<p className="bg-white text-lg">Cargando Competiciones...</p>} >
        <Await resolve={data.activeCompetitionGroups}>
          {(activeCompetitionGroups) =>
            <div className="flex flex-wrap justify-center">
              <Form.Select onChange={changeCompetitionGroup} value={data.activeCompetitionGroupId}>
                {activeCompetitionGroups.map((competitionGroup) => {
                  return (
                    <option
                      key={competitionGroup.id}
                      value={competitionGroup.id}
                      className={!competitionGroup.active ? 'text-slate-500': ''}
                    >
                      {competitionGroup.name}
                    </option>
                  )
                })}
              </Form.Select>
            </div>
          }
        </Await>
      </React.Suspense>
      <React.Suspense fallback={<><br/><p className="bg-white text-lg">Cargando Clasificaciones...</p></>} >
        <Await resolve={data.competitionClasifications}>
          {(competitionClasifications) =>
            <div className="flex flex-wrap justify-center">
              {competitionClasifications.length === 0 ? (
                <div className="border-solid border-2 border-orange-500 rounded bg-white px-2 py-2 mx-2 my-2">
             No hay competiciones
                </div>
              ) : (
                competitionClasifications.map(competitionClasification =>
                  <CompetitionClasificationCard 
                    key={competitionClasification[0].competition.id}
                    competitionClasification={competitionClasification} 
                  />))
              }
            </div>
          }
        </Await>
      </React.Suspense>
        
    </>
  )

}
